# Typography

Rentable Store uses two fonts, available from Google Fonts.
These are also available on NPM for use in applications.

### Bitter

```hint
Use for headings only, with respective color.
```

[Google Fonts](https://fonts.google.com/specimen/Bitter)
[NPM](https://npm.im/typeface-bitter)

```type
{
  "headings": [98,28,21,16],
  "font": "Bitter",
  "color": "#32325d"
}
```

### Open Sans

```hint
Use for all other text, including paragraphs, forms, etc.
```

[Google Fonts](https://fonts.google.com/specimen/Open+Sans)
[NPM](https://npm.im/typeface-open-sans)

```type
{
  "paragraphs": ["16"],
  "font": "Open Sans",
  "color": "#525f7f"
}
```
