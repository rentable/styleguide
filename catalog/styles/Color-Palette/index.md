# Rentable Default Theme

#### Colors

```color
value: '#6772e5'
name: 'Primary'
span: 2
```

```color
value: '#82d3f4'
name: 'Info'
span: 2
```

```color
value: '#3ecf8e'
name: 'Success'
span: 2
```

```color
value: '#fa755a'
name: 'Warning'
span: 2
```

```color
value: '#f51c47'
name: 'Danger'
span: 2
```

```color
value: '#ffffff'
name: 'Neutral'
span: 2
```

#### Text Colors

```color
value: '#525f7f'
name: 'Text Primary'
span: 2
```

```color
value: '#6772e5'
name: 'Text Link'
span: 2
```

```color
value: '#32325d'
name: 'Text Header'
span: 1
```

```color
value: '#8898aa'
name: 'Text Input'
span: 1
```

#### Gradients

Background Gradient

```code
background: linear-gradient(150deg, #281483 15%, #8f6ed5 70%, #d782d9 94%);
```

```color-palette|horizontal
colors:
  - {name: "15%", value: "#281483"}
  - {name: "70%", value: "#8f6ed5"}
  - {name: "94%", value: "#d782d9"}
```

```html
plain: true
showSource: true
noSource: true
---
<div style='height: 50vh; background: linear-gradient(150deg, #281483 15%, #8f6ed5 70%, #d782d9 94%);'></div>
```
