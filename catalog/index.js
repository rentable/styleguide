import React from "react";
import ReactDOM from "react-dom";
import { Catalog, pageLoader } from "catalog";

const pages = [
  {
    path: "/",
    title: "Welcome",
    content: pageLoader(() => import("./WELCOME.md"))
  },
  {
    title: 'Styles',
    pages: [
      {
        path: '/styles',
        title: 'Introduction',
        content: pageLoader(() => import("./styles/intro.md"))
      },
      {
        path: '/styles/color',
        title: 'Color Palette',
        content: pageLoader(() => import("./styles/Color-Palette/index.md"))
      },
      {
        path: '/styles/type',
        title: 'Typography',
        content: pageLoader(() => import("./styles/Typography/index.md"))
      }
    ]
  }
];

const theme = {
  fontFamily: 'Open Sans',
  fontHeading: 'Bitter',
  textColor: '#525f7f',
  codeColor: '#32325d',
  linkColor: '#6772e5',
  brandColor: '#6772e5',
  pageHeadingBackground: '#32325d',
  pageHeadingTextColor: '#fff',
  sidebarColorActive: "#6772e5",
  sidebarColorText: "#32325d",
  sidebarColorTextActive: "#6772e5",
  sidebarColorLine: "#EBEBEB",
  sidebarColorHeading: "#32325d",
  codeStyles: {
    tag: {color: '#32325d', fontWeight: 'bold'}
  }
}

ReactDOM.render(
  <Catalog title="Rentable" theme={theme} useBrowserHistory={true} pages={pages} logoSrc={'/Rentable_PNG_Black.png'} />,
  document.getElementById("catalog")
);
