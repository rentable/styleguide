```image
src: Rentable_SVG_Black.svg
plain: true
```

Welcome to the Rentable styleguide. This aims to setup the basic design styles that Rentable uses, including colors, typography, component styles etc.
Navigate through the site using the menu on the left.
